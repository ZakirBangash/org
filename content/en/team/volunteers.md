---
title: Volunteers
description: Grey Software Volunteer's
category: Team
postion: 2
volunteers:
  - name: Milind
    avatar: https://avatars.githubusercontent.com/u/48028572?s=460&u=7a0793800536db2a99dace6279495835f0550567&v=4
    position: Software Developer
    github: https://github.com/milindvishnoi
    gitlab: https://gitlab.com/milindvishnoi
    linkedin: https://www.linkedin.com/in/milindvishnoi/
  - name: Isha
    avatar: https://avatars.githubusercontent.com/u/56453971?s=460&v=4
    position: Operations Officer
    github: https://github.com/ishaaa-ai
    gitlab: https://gitlab.com/ishaaa-ai
    linkedin: https://www.linkedin.com/in/isha-kerpal-6a923819b/
  - name: Sidrah
    avatar: https://secure.gravatar.com/avatar/7bbd5dae8a5fb5d787f429478ec421c7?s=800&d=identicon
    position: Material Math
    github: https://github.com/degr8sid-code
    gitlab: https://gitlab.com/degr8sid-code
    linkedin: https://www.linkedin.com/in/hsabdullah/
  - name: Zainab
    avatar: https://secure.gravatar.com/avatar/85f9bf6728a3fdd2f1ae83509cc76702?s=800&d=identicon
    position: Material Math
    github: https://github.com/zainabnaveed
    gitlab: https://gitlab.com/zainabnaveed
    linkedin: https://www.linkedin.com/in/zainab-naveed-6b10871b6/
---

## Volunteers

<team-profiles :profiles="volunteers"></team-profiles>
